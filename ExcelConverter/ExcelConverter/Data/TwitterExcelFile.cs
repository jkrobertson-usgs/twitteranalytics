﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelConverter.Data
{
    public class TwitterExcelFile
    {
        public FileInfo Source { get; set; }
        public List<TwitterMetric> Data { get; set; }
    }
}
