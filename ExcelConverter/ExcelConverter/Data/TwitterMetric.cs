﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelConverter.Data
{
    public class TwitterMetric
    {
        public String TweetID { get; set; }
        public String TweetPermalink { get; set; }
        public String TweetText { get; set; }
        public DateTime Time { get; set; }
        public int Impressions { get; set; }
        public int Engagements { get; set; }
        public Double EngagementRate { get; set; }
        public int Retweets { get; set; }
        public int Replies { get; set; }
        public int Likes { get; set; }
        public int UserProfileClicks { get; set; }
        public int URLClicks { get; set; }
        public int HashTagClicks { get; set; }
        public int DetailExpands { get; set; }
        public int Permalink { get; set; }
        public int AppOpens { get; set; }
        public int AppInstalls { get; set; }
        public int Follows { get; set; }
        public int EmailTweet { get; set; }
        public int DialPhone { get; set; }
        public int MediaViews { get; set; }
        public int MediaEngagements { get; set; }

        public bool IsAutomatedTweet()
        {
            if (TweetText.StartsWith("#USGS")) return true; else return false;
        }

        public String GetSiteNumber()
        {
            if(IsAutomatedTweet())
            {
                //Automated tweets start like #USGS08041700 -
                var spaceIndex = TweetText.IndexOf(" -");
                var siteNumber = TweetText.Substring(5, spaceIndex - 5);

                return siteNumber;
            }
            return "";
        }

        public String GetStationName()
        {
            if (IsAutomatedTweet())
            {
                //Automated tweets start like #USGS08041700 - Pine Island Bayou nr Sour Lake, TX 
                var startIndex = TweetText.IndexOf(" - ") + 3;
                var endIndex = TweetText.ToLower().IndexOf("tx");
                var stationName = TweetText.Substring(startIndex, endIndex - startIndex + 2);

                return stationName;
            }
            return "";
        }

    }
}
