﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
namespace ExcelConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var outputExcelFilePath = GetOutputFileName();
                var outPutFile = new FileInfo(outputExcelFilePath);
                Console.WriteLine("Looking for excel files in Staging Directory.\n");
                var excelFiles = GetStagingFiles();
                var twitterFiles = new List<Data.TwitterExcelFile>();
                // Check that there are excel files to process
                if(excelFiles.Count == 0)
                {
                    Console.WriteLine("No excel files were found.");
                    Console.WriteLine("Press any key to escape...");
                    Console.ReadLine();
                    return;
                }
                // Getting all data from all excel files in the staging folder
                foreach (var file in excelFiles)
                {
                    Console.WriteLine($"Processing File: { file.Name} ");
                    var data = GetDataFromExcelFile(file);
                    twitterFiles.Add(new Data.TwitterExcelFile()
                    {
                        Data = data,
                        Source = file
                    });
                }
                Console.WriteLine($"\nWriting Data to {outPutFile.Name}");
                WriteDataToExcel(outPutFile, twitterFiles);

                Console.WriteLine("Job Complete.\nPress any key to escape...");
            }catch(Exception ex)
            {
                Console.WriteLine("Ooops, something went wrong check the error message below");
                Console.WriteLine(ex.Message);
                Console.WriteLine("Press any key to escape...");
            }
            
            Console.ReadLine();
        }
        /// <summary>
        /// A function made to create a unique file name for the output file
        /// </summary>
        /// <returns>The path to the output file</returns>
        static String GetOutputFileName()
        {
            // Get the directory this executable is running in
            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var current = DateTime.Now;
            var fileName = $"twitter_output_{current.Year}_{current.Month:00}_{current.Day:00}_{current.Ticks}.xlsx";

            var completedPath = Path.Combine(rootDirectory, "Completed", fileName);

            return completedPath;
        }
        /// <summary>
        /// This is a function that will return a list of file information for all the excel files in the staging directory
        /// </summary>
        /// <returns>A list of file information of excel files that need to be processed</returns>
        static List<FileInfo> GetStagingFiles()
        {
            var ret = new List<FileInfo>();
            // Get the directory this executable is running in
            var rootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var stagingDirectory = Path.Combine(rootDirectory, "Staging");

            var files = Directory.GetFiles(stagingDirectory).Where(p => p.EndsWith(".xlsx")).ToList();
            if(files.Count > 0)
            {
                Console.WriteLine("Excel Files Found:");
            }
            foreach(var file in files)
            {
                var fileInfo = new FileInfo(file);
                Console.WriteLine($"{fileInfo.Name}");
                ret.Add(fileInfo);
            }

            return ret;
        }
        /// <summary>
        /// This is a function that will take an excel document and return a list of twitter metrics for each row in that excel document
        /// </summary>
        /// <param name="excelFile">The file information for a xlsx file</param>
        /// <returns>The data from that excel file as a list of twitter metrics</returns>
        static List<Data.TwitterMetric> GetDataFromExcelFile(FileInfo excelFile)
        {
            var ret =  new List<Data.TwitterMetric>();
            // Read file as binary into memory
            using (FileStream file = excelFile.OpenRead())
            {
                // Create an excel package from binary in memory stream
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    var workBook = package.Workbook;
                    var workSheet = workBook.Worksheets.FirstOrDefault();
                    var endRow = workSheet.Dimension.End.Row;
                    for(int rowIDX = 2; rowIDX <= endRow; rowIDX++)
                    {
                        //Iterate row by row
                        var metric = new Data.TwitterMetric();

                        #region Filling Metric with Data
                        metric.TweetID = workSheet.Cells[rowIDX, 1].Text;
                        metric.TweetPermalink = workSheet.Cells[rowIDX, 2].Text;
                        metric.TweetText = workSheet.Cells[rowIDX, 3].Text;
                        var time = workSheet.Cells[rowIDX, 4].Text;
                        time = time.Replace("+0000", "");
                        metric.Time = Convert.ToDateTime(time);
                        metric.Impressions = Convert.ToInt32(workSheet.Cells[rowIDX, 5].Text);
                        metric.Engagements = Convert.ToInt32(workSheet.Cells[rowIDX, 6].Text);
                        metric.EngagementRate = Convert.ToDouble(workSheet.Cells[rowIDX, 7].Text);
                        metric.Retweets = Convert.ToInt32(workSheet.Cells[rowIDX, 8].Text);
                        metric.Replies = Convert.ToInt32(workSheet.Cells[rowIDX, 9].Text);
                        metric.Likes = Convert.ToInt32(workSheet.Cells[rowIDX, 10].Text);
                        metric.UserProfileClicks = Convert.ToInt32(workSheet.Cells[rowIDX, 11].Text);
                        metric.URLClicks = Convert.ToInt32(workSheet.Cells[rowIDX, 12].Text);
                        metric.HashTagClicks = Convert.ToInt32(workSheet.Cells[rowIDX, 13].Text);
                        metric.DetailExpands = Convert.ToInt32(workSheet.Cells[rowIDX, 14].Text);
                        metric.Permalink = Convert.ToInt32(workSheet.Cells[rowIDX, 15].Text);
                        metric.AppOpens = Convert.ToInt32(workSheet.Cells[rowIDX, 16].Text);
                        metric.AppInstalls = Convert.ToInt32(workSheet.Cells[rowIDX, 17].Text);
                        metric.Follows = Convert.ToInt32(workSheet.Cells[rowIDX, 18].Text);
                        metric.EmailTweet = Convert.ToInt32(workSheet.Cells[rowIDX, 19].Text);
                        metric.DialPhone = Convert.ToInt32(workSheet.Cells[rowIDX, 20].Text);
                        metric.MediaViews = Convert.ToInt32(workSheet.Cells[rowIDX, 21].Text);
                        metric.MediaEngagements = Convert.ToInt32(workSheet.Cells[rowIDX, 22].Text);
                        #endregion

                        ret.Add(metric);
                    }

                }
            }

            return ret;
        }

        static void WriteDataToExcel(FileInfo outputFile, List<Data.TwitterExcelFile> excelFiles)
        {

            using (ExcelPackage package = new ExcelPackage())
            {
                var workBook = package.Workbook;

                foreach(var excelFile in excelFiles)
                {
                    var feedName = GetFeedNameFromURL(excelFile.Data.FirstOrDefault().TweetPermalink);
                    var tabName = GetTabName(feedName, excelFile.Data.Min(p => p.Time), excelFile.Data.Max(p => p.Time));
                    //TexasRain_20170501_
                    ExcelWorksheet comments = null;
                    ExcelWorksheet regular = null;

                    var regularRowIndex = 2;
                    var commentRowIndex = 2;
                    // Assign header text

                    // Loop through data and put data into cells
                    foreach(var data in excelFile.Data)
                    {
                        if(data.IsAutomatedTweet())
                        {
                            if (regular == null)
                            {
                                regular = package.Workbook.Worksheets.Add(tabName);
                            }
                            // Goes into regular worksheet
                            regular.Cells[regularRowIndex, 1].Value = data.TweetPermalink;
                            regular.Cells[regularRowIndex, 2].Value = data.GetSiteNumber();
                            regular.Cells[regularRowIndex, 3].Value = data.GetStationName();
                            regular.Cells[regularRowIndex, 4].Value = String.Format("{0:MM/dd/yyyy}", data.Time);
                            regular.Cells[regularRowIndex, 5].Value = data.Time.ToString("HH:mm");
                            regular.Cells[regularRowIndex, 6].Value = data.Impressions;
                            regular.Cells[regularRowIndex, 7].Value = data.Engagements;
                            regular.Cells[regularRowIndex, 8].Value = data.Retweets;
                            regular.Cells[regularRowIndex, 9].Value = data.Replies;
                            regular.Cells[regularRowIndex, 10].Value = data.Likes;
                            regular.Cells[regularRowIndex, 11].Value = data.UserProfileClicks;
                            regular.Cells[regularRowIndex, 12].Value = data.URLClicks;
                            regular.Cells[regularRowIndex, 13].Value = data.HashTagClicks;
                            regular.Cells[regularRowIndex, 14].Value = data.DetailExpands;
                            regular.Cells[regularRowIndex, 15].Value = data.Permalink;
                            regular.Cells[regularRowIndex, 16].Value = data.Follows;

                            regularRowIndex++;

                        }
                        else
                        {
                            if(comments == null)
                            {
                                comments = package.Workbook.Worksheets.Add($"{tabName} Comments");
                            }
                            // Goes into the comment worksheet
                            comments.Cells[commentRowIndex, 1].Value = data.TweetPermalink;
                            comments.Cells[commentRowIndex, 2].Value = data.GetSiteNumber();
                            comments.Cells[commentRowIndex, 3].Value = data.GetStationName();
                            comments.Cells[commentRowIndex, 4].Value = String.Format("{0:MM/dd/yyyy}", data.Time);
                            comments.Cells[commentRowIndex, 5].Value = data.Time.ToString("HH:mm");
                            comments.Cells[commentRowIndex, 6].Value = data.Impressions;
                            comments.Cells[commentRowIndex, 7].Value = data.Engagements;
                            comments.Cells[commentRowIndex, 8].Value = data.Retweets;
                            comments.Cells[commentRowIndex, 9].Value = data.Replies;
                            comments.Cells[commentRowIndex, 10].Value = data.Likes;
                            comments.Cells[commentRowIndex, 11].Value = data.UserProfileClicks;
                            comments.Cells[commentRowIndex, 12].Value = data.URLClicks;
                            comments.Cells[commentRowIndex, 13].Value = data.HashTagClicks;
                            comments.Cells[commentRowIndex, 14].Value = data.DetailExpands;
                            comments.Cells[commentRowIndex, 15].Value = data.Permalink;
                            comments.Cells[commentRowIndex, 16].Value = data.Follows;

                            commentRowIndex++;
                        }
                    }
                    Dictionary<int, String> headers = new Dictionary<int, string>()
                    {
                        { 1, "Tweet Permalink" },
                        { 2, "Station ID"  },
                        { 3, "Station Name"  },
                        { 4, "Date"  },
                        { 5, "Time"  },
                        { 6, "Impressions"  },
                        { 7, "Engagements"  },
                        { 8, "Retweets"  },
                        { 9, "Replies"  },
                        { 10, "Likes"  },
                        { 11, "User Profile Clicks"  },
                        { 12, "URL Clicks"  },
                        { 13, "Hashtag Clicks"  },
                        { 14, "Detail Expands"  },
                        { 15, "Permalink Clicks"  },
                        { 16, "Follows"  }
                    };
                    if(regular != null)
                    {
                        foreach(var header in headers)
                        {
                            regular.Cells[1, header.Key].Value = header.Value;
                            regular.Cells[1, header.Key].Style.Font.Bold = true;
                            regular.Cells[1, header.Key].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            regular.Cells[1, header.Key].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            regular.Cells[1, header.Key].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        }
                        regular.Cells[regular.Dimension.Address].AutoFitColumns();
                    }
                    if(comments != null)
                    {
                        foreach (var header in headers)
                        {
                            comments.Cells[1, header.Key].Value = header.Value;
                            comments.Cells[1, header.Key].Style.Font.Bold = true;
                            comments.Cells[1, header.Key].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Thin);
                            comments.Cells[1, header.Key].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            comments.Cells[1, header.Key].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        }
                        comments.Cells[comments.Dimension.Address].AutoFitColumns();
                    }
                }

                package.SaveAs(outputFile);
            }
        }

        /// <summary>
        /// This function returns the name of the tab in the excel document
        /// </summary>
        /// <param name="feedName">The name of the twitter feed example: TX_Flood</param>
        /// <param name="min">The oldest date of a record</param>
        /// <param name="max">The newest date of a record</param>
        /// <returns></returns>
        static String GetTabName(String feedName, DateTime min, DateTime max)
        {
            if(max.Year == min.Year)
            {
                // We know both dates have the same year
                if(max.Month == min.Month)
                {
                    // We know both dates have the same year and month
                    return $"{feedName} {min:MMMM} {min.Year}";
                }
            }
            return $"{feedName} {min:MMMMM} {min.Year} to {max:MMMMM} {max.Year}";
        }

        static String GetFeedNameFromURL(String url)
        {
            // url looks like https://twitter.com/USGS_TexasRain/status/870066504464646144
            url = url.Replace("https://twitter.com/", "");
            url = url.Substring(0, url.IndexOf('/'));
            return url;
        }
    }
}
